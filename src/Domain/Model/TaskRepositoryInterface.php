<?php

namespace App\Domain\Model;

/**
 * Class TaskRepositoryInterface
 * @package App\Domain\Model
 */
interface TaskRepositoryInterface
{
    public function salvar(Task $task): void;
}
